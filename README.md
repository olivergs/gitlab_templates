# Gitlab Templates

Repository to store Gitlab CI templates.

Following the [DRY pattern](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself), we can use [templates](https://docs.gitlab.com/ee/development/cicd/templates.html).

This repository contains common jobs to use in our repositories.

By default, the following stages are defined:

* setup
* lint
* test
* build
* publish

## Template files

Every file should be responsible for a **single functionality**. By doing this, we are keeping our files clean, readable, and maintainable. All templates files are under *.gitlab* directory

```bash
$ tree .gitlab
.gitlab
├── all_templates.yml
├── base_templates
│   ├── default.yml
│   ├── security.yml
│   ├── stages.yml
│   └── workflow.yml
└── ci_templates
    ├── bash.yml
    ├── common.yml
    ├── containers.yml
    └── python.yml
```

## Container images

All templates are executed with containers, for every job template I've declared at least two variables

* `<JOB_TEMPLATE_NAME>`_CONTAINER_IMAGE
* `<JOB_TEMPLATE_NAME>`_CONTAINER_VERSION

If you want to replace any container image or version, you only need to change any of this variables. For example in `.markdownlint`, you should overwrite this variables with your custom values.

* MARKDOWNLINT_CONTAINER_IMAGE
* MARKDOWNLINT_CONTAINER_VERSION

### [Base templates](.gitlab/base_templates)

Base templates are used by ci template files, we should not import base templates.

* [default](.gitlab/base_templates/default.yml): Includes all values under *default* keyword.
* [workflow](.gitlab/base_templates/workflow.yml): Includes all values under *workflow* keyword.
* [stages](.gitlab/base_templates/stages.yml): Includes common stages (keyword *stages*).
* [security](.gitlab/base_templates/security.yml): Includes security stuff.

### [CI templates](./gitlab/ci_templates)

Those templates should be imported into `gitlab-ci.yml` files in other repositories.

### [common](.gitlab/ci_templates/common.yml)

This file contains the following *job templates*:

* *.markdownlint*: Search recursively mardown files and execute [markdownlint](https://github.com/DavidAnson/markdownlint). If a *.mdlrc* file exists at root of the repository, *markdownlint* will use it.
* *.release*: Using gitlab API create a release in the project, when a new tag is created.
* *.yamllint*: Search recursively *yaml* files and execute [yamllint](https://yamllint.readthedocs.io/en/stable/). If a *.yamllint* file exists at root of the repository, *yamllint* will use it.

Example:

```yaml
---
include:
  - project: kamaxeon/gitlab_templates
    file: .gitlab/ci_templates/common.yml

markdownlint:
  extends: .markdownlint

yamllint:
  extends: .yamllint

release:
  extends: .release
  variables:
    CONTAINER_IMAGE_NAME: "container image"
    CONTAINER_IMAGE_URL: "docker_registry/image:${CI_COMMIT_REF_NAME}"
  script:
    - >
      release-cli create --name release-branch-$CI_JOB_ID --description release-branch-$CI_COMMIT_REF_NAME-$CI_JOB_ID
      --tag-name job-$CI_JOB_ID --ref $CI_COMMIT_SHA
      -assets-link '{"name":"${CONTAINER_IMAGE_NAME}","url":"${CONTAINER_IMAGE_URL}"}'
```

### [bash](.gitlab/ci_templates/bash.yml)

Bash file includes common *job templates* and contains the following *job templates*:

* *.shellcheck*: Searches recursively bash files and execute [shellcheck](https://github.com/koalaman/shellcheck).
* *.shellspec*: Searches shellspec test files and run [shellspec](https://shellspec.info/) with coverage support using [kcov](https://simonkagstrom.github.io/kcov/).

Example:

```yaml
---
include:
  - project: kamaxeon/gitlab_templates
    file: .gitlab/ci_templates/bash.yml

shellcheck:
  extends: .shellcheck

shellspec:
  extends: .shellspec
```

### [containers](.gitlab/ci_templates/containers.yml)

Container file includes common *job templates* and contains the following *job templates*:

* *.hadolint*: Linter for container files. If a *.hadolint.yaml* file exists at root path where the job is executed, [hadolint](https://github.com/hadolint/hadolint) will use it.
* *.container_build*: Builds a container images using [buildah](https://buildah.io/).
* *.container_publish*: Publishes a container image in the gitlab registry of your project.

All container files **must** be in *builds* folder, all the templates expect the variable *$CONTAINER_IMAGE*.
The value of this variable **must** the name of the container, and the container file name must match this pattern
**$CONTAINER_IMAGE.Dockerfile**, for example for *my_awesome_app* container image, the filename should be **my_awesome_app.Dockerfile**

By default, container images will be uploaded to the gitlab registry of your gitlab instances, if you want to use another one, for example quay.io, you only need to change some values.

Here you can see all variables and values by default

```yaml
  CONTAINER_REGISTRY_USERNAME: $CI_REGISTRY_USER
  CONTAINER_REGISTRY_PASSWORD: $CI_REGISTRY_PASSWORD
  CONTAINER_REGISTRY: $CI_REGISTRY
  CONTAINER_REGISTRY_IMAGE: $CI_REGISTRY_IMAGE
````

Example of use:

```yaml
---
include:
  - project: kamaxeon/gitlab_templates
    file: .gitlab/ci_templates/containers.yml

# I want to create two images
# * python
# * shellspec
.container_images:
  parallel:
    matrix:
      - CONTAINER_IMAGE: [python, shellspec]

hadolint:
  extends: [.hadolint, .container_images]

container_build:
  extends: [.container_build, .container_images]

container_publish:
  extends: [.container_publish, .container_images]
```

### [python](.gitlab/ci_templates/python.yml)

Python file includes common *job templates* and contains the following *job templates*:

* *.flake8*: Through tox runs [flake8](https://flake8.pycqa.org/en/latest/).
* *.isort*: Through tox runs [isort](https://pycqa.github.io/isort/).
* *.pydocstyle*: Through tox runs [pydocstyle](http://www.pydocstyle.org/en/stable/).
* *.pylint*: Through tox runs [pylint](https://pylint.pycqa.org/en/latest/).
* *.python_coverage*: Through tox runs [coverage](https://coverage.readthedocs.io/en/latest/) and sends results to gitlab.
* *.mypy*: Through tox runs [mypy](http://mypy-lang.org/).

[Python slim](https://hub.docker.com/_/python) is used as base image for python jobs. We encourage to use *setup.cfg* file to modify any parameter of any *job template*.

Example:

```yaml
---
include:
  - project: kamaxeon/gitlab_templates
    file: .gitlab/ci_templates/python.yml

flake8:
  extends: .flake8

pylint:
  extends: .pylint

isort:
  extends: .isort

pydocstyle:
  extends: .pydocstyle

coverage:
  extends: .python_coverage

mypy:
  extends: .mypy
```

Here is an example of a basic `setup.cfg`

```ini
[metadata]
name = my_project
description = Awesome project
author = Israel Santana <isantana@redhat.com>
license = GPLv2+
version = 0.1.0

[options]
packages = my_project
include_package_data = True
install_requires =
    library_1
    library_2

[tox:tox]
envlist = flake8,pylint,isort,pydocstyle,coverage

[testenv:flake8]
deps = flake8
commands = flake8 my_project tests

[flake8]
exclude = .tox,build,dist,.eggs,venv,.venv
max-line-length = 100

[testenv:pylint]
deps = pylint
commands = pylint my_project tests

[testenv:isort]
deps = isort
commands = isort --check --diff my_project tests

[testenv:pydocstyle]
deps = pydocstyle
commands = pydocstyle --match-dir='^(?!(\.|build|dist|venv|.venv)).*'

[testenv:coverage]
deps =
    pytest
    pytest-subtests
    pytest-cov
    coverage
commands =
  coverage run --source my_project --branch -m pytest --junitxml=coverage/junit.xml --color=yes -v
  coverage report -m
  coverage html -d coverage/
  coverage xml -o coverage/coverage.xml
```

Here is an example for setup.pytest

```python
"""Setup."""

from setuptools import setup

setup()
```

### [All templates](.gitlab/all_templates.yml)

This file contains includes all ci templates, so we only need to import this file to use all *job templates*.

Example:

```yaml
---
include:
  - project: kamaxeon/gitlab_templates
    file: .gitlab/all_templates.yml

shellcheck:
  extends: .shellcheck
```
