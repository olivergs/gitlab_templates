---
# File containers.yml
# This file contains containers stuff

# Based on
# https://blog.callr.tech/building-docker-images-with-gitlab-ci-best-practices/

# Include common file
include:
  - /.gitlab/ci_templates/common.yml

# List of variables used.
variables:
  IMAGE_TAG: $CI_COMMIT_SHA
  BUILDAH_CONTAINER_IMAGE: quay.io/buildah/stable
  BUILDAH_CONTAINER_VERSION: latest
  HADOLINT_CONTAINER_IMAGE: registry.gitlab.com/pipeline-components/hadolint
  HADOLINT_CONTAINER_VERSION: latest
  CONTAINER_BUILD_CONTAINER_IMAGE: $BUILDAH_CONTAINER_IMAGE
  CONTAINER_BUILD_CONTAINER_VERSION: $BUILDAH_CONTAINER_VERSION
  CONTAINER_PUBLISH_CONTAINER_IMAGE: $BUILDAH_CONTAINER_IMAGE
  CONTAINER_PUBLISH_CONTAINER_VERSION: $BUILDAH_CONTAINER_VERSION
  CONTAINER_REGISTRY_USERNAME: $CI_REGISTRY_USER
  CONTAINER_REGISTRY_PASSWORD: $CI_REGISTRY_PASSWORD
  CONTAINER_REGISTRY: $CI_REGISTRY
  CONTAINER_REGISTRY_IMAGE: $CI_REGISTRY_IMAGE

# Templates to export

## Linter for container files.
## We need to declare "$CONTAINER_IMAGE"
.hadolint:
  image: $HADOLINT_CONTAINER_IMAGE:$HADOLINT_CONTAINER_VERSION
  stage: lint
  before_script:
    - hadolint --version
  script:
    - hadolint builds/$CONTAINER_IMAGE.Dockerfile

## Build a container image.
## We need to declare "$CONTAINER_IMAGE"
.container_build:
  extends: .container_registry
  image: $CONTAINER_BUILD_CONTAINER_IMAGE:$CONTAINER_BUILD_CONTAINER_VERSION
  stage: build
  script:
    # fetches the latest image (not failing if image is not found)
    - buildah pull $CI_REGISTRY_IMAGE/$CONTAINER_IMAGE:latest || true
    - >
      buildah bud
      --pull
      --cache-from $CONTAINER_REGISTRY_IMAGE/$CONTAINER_IMAGE:latest
      --label "org.opencontainers.image.title=$CONTAINER_IMAGE"
      --label "org.opencontainers.image.url=$CI_PROJECT_URL"
      --label "org.opencontainers.image.created=$CI_JOB_STARTED_AT"
      --label "org.opencontainers.image.revision=$CI_COMMIT_SHA"
      --label "org.opencontainers.image.version=$CI_COMMIT_REF_NAME"
      --tag  $CONTAINER_REGISTRY_IMAGE/$CONTAINER_IMAGE:$CI_COMMIT_SHA
      --file builds/$CONTAINER_IMAGE.Dockerfile
      .
    - buildah push $CONTAINER_IMAGE_REGISTRY_IMAGE/$CONTAINER_IMAGE:$CI_COMMIT_SHA
    - echo "New $CONTAINER_IMAGE image available at
            $CONTAINER_REGISTRY_IMAGE/$CONTAINER_IMAGE:$CI_COMMIT_SHA"

## Publish a container image.
## We need to declare "$CONTAINER_IMAGE"
.container_publish:
  extends: .container_registry
  image: $CONTAINER_PUBLISH_CONTAINER_VERSION:$CONTAINER_PUBLISH_CONTAINER_VERSION
  stage: publish
  variables:
    GIT_STRATEGY: none
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      variables:
        IMAGE_TAG: mr-$CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
      variables:
        IMAGE_TAG: $CI_COMMIT_REF_NAME
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      variables:
        IMAGE_TAG: latest
  script:
    - buildah pull $CONTAINER_REGISTRY_IMAGE/$CONTAINER_IMAGE:$CI_COMMIT_SHA
    - buildah tag $CONTAINER_REGISTRY_IMAGE/$CONTAINER_IMAGE:$CI_COMMIT_SHA
       $CONTAINER_REGISTRY_IMAGE/$CONTAINER_IMAGE:$IMAGE_TAG
    - buildah push $CONTAINER_REGISTRY_IMAGE/$CONTAINER_IMAGE:$IMAGE_TAG
    - echo "New $CONTAINER_IMAGE image available at
            $CONTAINER_REGISTRY_IMAGE/$CONTAINER_IMAGE:$IMAGE_TAG"

# Internal helper templates
## Helper to login and logout
.container_registry:
  image: $BUILDAH_CONTAINER_IMAGE:$BUILDAH_CONTAINER_VERSION
  before_script:
    - buildah --version
    - echo -n $QUAY_REGISTRY_PASSWORD | buildah login
      -u $CONTAINER_REGISTRY_USER --password-stdin $CONTAINER_REGISTRY
  after_script:
    - buildah logout $CONTAINER_REGISTRY
